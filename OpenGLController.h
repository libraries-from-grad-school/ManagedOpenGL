//
//  OpenGLController.h
//  Forest Map NCD
//
//  Created by William Dillon on 11/24/06.
//  Copyright 2006 VIA Computing. All rights reserved.
//  Permission granted to Oregon State University for all use.
//

#import <Cocoa/Cocoa.h>
#import "OpenGLController.h"
#import "OpenGL/gl.h"
#import "ShaderProgram.h"

@class OpenGLView;

@interface OpenGLController : NSObject {
	IBOutlet OpenGLView *openGLView;
		
	// Projection tramsform values
	GLdouble scale;
	GLfloat  aspectRatio;
	
	// Old mouse location for dragging
	NSPoint oldMouse;

	// Screen refresh timer
	NSTimer *timer;
}

// Get initialization parameters
+ (NSOpenGLPixelFormat *)defaultPixelFormat;

// UI IBActions and Events
- (IBAction)updateData:(id)sender;

- (void)mouseDownLocation:(NSPoint)location Flags:(unsigned int)modifierFlags;
- (void)mouseDraggedLocation:(NSPoint)location Flags:(unsigned int)modifierFlags;
- (void)scrollWheel:(NSEvent *)theEvent;

// Setters
- (void)setAspectRatio:(GLfloat)aRatio;
- (void)setView:(id)sender;

// OpenGL Methods and callbacks
- (void)initGL;
- (void)reshape:(NSRect)rect;
- (void)draw;
- (void)requestRedraw:(NSTimer*)theTimer;

@end
